import { decode } from "https://esm.sh/html-entities?pin=v55";

const clientId = Deno.env.get('CLIENT_ID')
const clientIntegrationId = Deno.env.get('CLIENT_INTEGRATION_ID')
const clientEcommerce = JSON.parse(Deno.env.get('CLIENT_ECOMMERCE'))
const partial = Deno.env.get('PARTIAL')

let data = await fetch(`https://s3-sa-east-1.amazonaws.com/imagenes-loiuruguay/sagal/produccion/datos.json`);
let json = await data.json()


async function init() {
    let payloads = []

    for (let elem of json) {
        if (elem.code != '') {
            try {
                let payload = createArticle(elem)
                payloads.push(payload)
            } catch (e) {
                console.log(`Could not sync ${elem.code}: ${e.message}`)
            }
        }

    }
    await sagalDispatchBatch({
        products: payloads,
        batch_size: 100
    })
}

function createArticle(elem) {
    let payload = {
        sku: elem.code,
        client_id: clientId,
        integration_id: clientIntegrationId,
        options: {
            merge: false,
            partial: partial === "true"
        },
        ecommerce: Object.values(clientEcommerce).map(ecommerce_id => {
            let data = {
                ecommerce_id: ecommerce_id,
                options: {
                    override_create: "default",
                    override_update: {
                        "send": false
                    },
                },
                properties: [
                    {
                        "storestock": Object.keys(elem.stock).reduce(((accumulator, value) => {
                            accumulator[parseInt(value)] = elem.stock[value] === "0" ? { value: 0 } : { value: 999.0 }
                            return accumulator
                        }), {})
                    },
                    { "name": elem.name },
                    {
                        "price": {
                            "value": elem.price,
                            "currency": elem.currency === "1" ? "U$S" : "$",
                        },
                    },
                    { "images": elem.pictures },
                    { "description": decode(elem.description + (elem.ficha ? "\n" + elem.ficha : "")) },
                    {
                        "metadata": {
                            "state": elem.state
                        }
                    },
                    {
                        "stock": 0
                    }
                ],
                variants: []
            }

            if (elem.variations && elem.variations.length) {
                data.variants = elem.variations.map(x => {
                    let data = {
                        sku: x.code,
                        properties: [
                            {
                                "attributes": {
                                    "COLOR": x.colour,
                                }
                            },
                            { "images": x.pictures },
                            {
                                "metadata": {
                                    "state": x.state
                                }
                            },
                            {
                                "stock": 0
                            }
                        ]
                    }

                    return data
                })

                let variantAsSelf = {
                    sku: elem.code,
                    properties: [
                        {
                            "attributes": {
                                "COLOR": elem.colour,
                            }
                        },
                        { "images": elem.pictures },
                        {
                            "metadata": {
                                "state": elem.state
                            }
                        },
                        {
                            "stock": 0
                        }
                    ]
                }
                data.variants.push(variantAsSelf)
            }
            return data
        })
    }
    return payload
}

await init()